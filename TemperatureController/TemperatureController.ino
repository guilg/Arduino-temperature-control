/* ====================================================================
Peltier temperature controller with closed-loop temperature regulation.
Adapted from ThermoMaster by Raphael Candelier.
===================================================================== */

// --- INCLUDES --------------------------------------------------------

#include <Adafruit_MAX31856.h> // Thermocouples
#include <SPI.h>
#include <Chrono.h> // Timestamp
#include <PID_v1.h>

// --- PINOUTS ---------------------------------------------------------

// --- Temperature sensors

// NB: Use of Hardware SPI:
// pin 11 (ISCP 4) is MOSI/SDI
// pin 12 (ICSP 1) is MISO/SDO
// pin 13 (ISCP 3) is CLK
const int pThermocouple = 2;

// --- Peltier control (PWM compatible pins)
const int pPeltierH = 5;    // Hot
const int pPeltierC = 6;   // Cold

// --- DEFINITIONS ----------------------------------------------------

double Temperature;
float CJ;

boolean bRegul = false;
boolean bDirect = false;

double Target;          // Target temperature

float Pcoeff = 50;            // P
float Icoeff = 0.1;          // I
float Dcoeff = 0.5;          // D

double Cmd;            // Command to peltier module

// Handle inputs
String cmd0;
String cmd1;
String cmd2;
String cmd3;
int ind0;
int ind1;
int ind2;

unsigned long t;    // Time

// --- INITIALIZATIONS ------------------------------------------------

// --- PID
PID myPID(&Temperature, &Cmd, &Target, Pcoeff, Icoeff, Dcoeff, DIRECT);

// --- Thermocouples
Adafruit_MAX31856 TC = Adafruit_MAX31856(pThermocouple);

// --- Chronometre
Chrono Timestamp;

void setup() { 
  
  // --- Serial communication
  Serial.begin(115200);
  Serial.setTimeout(1);

  // --- Peltier modules
  pinMode(pPeltierH, OUTPUT);
  pinMode(pPeltierC, OUTPUT);
  analogWrite(pPeltierH, 0);
  analogWrite(pPeltierC, 0);

  // --- Temperature sensors
  TC.begin();
  TC.setThermocoupleType(MAX31856_TCTYPE_T);
  Temperature = TC.readThermocoupleTemperature();
  
  // --- PID
  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(-255, 255);
}

void loop() {
    
  // === INPUTS ======================================================
    
  if (Serial.available()) {   
  
    String cmd = Serial.readString();
    cmd.trim();
      
   // --- Identifier & Infos
    if (cmd.equals("getId")) {
      switch (TC.getThermocoupleType() ) {
        case MAX31856_TCTYPE_K: Serial.println("PeltierController with K Type"); break;
        case MAX31856_TCTYPE_T: Serial.println("PeltierController with T Type"); break;
        default: Serial.println("PeltierController with unknown Type"); break;
      }
    }
    
    if (cmd.equals("info")) {
      Serial.print("Info ");
      if (bRegul) { Serial.print("Regulating ");}
      else {Serial.print("NotRegulating ");}
      Serial.print(String(Target) + " ");
      Serial.print(String(Pcoeff) + " ");
      Serial.print(String(Icoeff) + " ");
      Serial.println(String(Dcoeff));
    }
    
    // --- Direct control
    if (cmd.substring(0,1).equals("H")) {
      
      int val = cmd.substring(2).toInt();
      Serial.println("Heating at " + String(100*val/255) + "%");
      bRegul = false; 
      bDirect = true;
      Cmd = val;
      
    } else if (cmd.substring(0,1).equals("C")) {
      
      int val = cmd.substring(2).toInt();
      Serial.println("Cooling at " + String(100*val/255) + "%");
      bRegul = false; 
      bDirect = true;
      Cmd = -val;
      
    }
    
    // --- Regulation
    if (cmd.equals("start")) {
      bRegul = true;
      bDirect = false;
    }
    
    if (cmd.equals("stop")) { bRegul = false; bDirect = false; }

    ind0 = cmd.indexOf('|');
    ind1 = cmd.indexOf('|',ind0+1);
    ind2 = cmd.indexOf('|',ind1+1);
    
    cmd0 = cmd.substring(0,ind0);
    cmd1 = cmd.substring(ind0+1,ind1);
    cmd2 = cmd.substring(ind1+1,ind2);
    cmd3 = cmd.substring(ind2+1);
    
    
    // Only one PID coefficient given
    if (ind0 == -1) {
      if (cmd.substring(0,1).equals("P")) { Pcoeff = cmd.substring(2).toFloat(); }
      if (cmd.substring(0,1).equals("I")) { Icoeff = cmd.substring(2).toFloat(); }
      if (cmd.substring(0,1).equals("D")) { Dcoeff = cmd.substring(2).toFloat(); }
    }

    // If several coefficient given and a target
    if (cmd0.substring(0,1).equals("T")) { Target = cmd0.substring(2).toFloat(); bRegul = true; bDirect = false; }
    if (cmd1.substring(0,1).equals("P")) { Pcoeff = cmd1.substring(2).toFloat(); }
    if (cmd2.substring(0,1).equals("I")) { Icoeff = cmd2.substring(2).toFloat(); }
    if (cmd3.substring(0,1).equals("D")) { Dcoeff = cmd3.substring(2).toFloat(); }

    // --- Set new P, I, D
    myPID.SetTunings(Pcoeff, Icoeff, Dcoeff);
    
    // --- Set target temperatures
    if (cmd.substring(0,3).equals("set")) {
      
      Target = cmd.substring(4).toFloat();
    
      // Debug
      /* Serial.print(Target);
      Serial.print(Pcoeff);
      Serial.print(" ");
      Serial.print(Icoeff);
      Serial.print(" ");
      Serial.println(Dcoeff);
      */
    }
  }

  // === MEASUREMENT =================================================
  Temperature = TC.readThermocoupleTemperature();
  CJ = TC.readCJTemperature();
  t = Timestamp.elapsed();

  // Send data to the program
  Serial.print("Data ");
  Serial.print(t);
  Serial.print(" ");
  Serial.print(Temperature, 8);
  Serial.print(" ");
  Serial.print(CJ);
  Serial.print(" ");
  Serial.println(Cmd);

  // === CONTROL ====================================================
 
  if (bRegul) {

    float gap = abs(Target - Temperature);
    
    // --- Get new commands
    myPID.Compute();

    // --- Adaptive control
    if (Cmd<0 and gap > 0.5) {Cmd = 2*Cmd;}

    // --- Boundaries
    if (Cmd<-255) {Cmd = -255;}
    if (Cmd>255) {Cmd = 255;}
    

  } else if (!bDirect) {

      Cmd = 0;
      
  }
  
  // --- Apply commands
  if (Cmd>=0) {                    // heat
    analogWrite(pPeltierC, 0);
    analogWrite(pPeltierH, round(Cmd));
  } else {                         // cool
    analogWrite(pPeltierH, 0); 
    analogWrite(pPeltierC, round(-Cmd));
  }
  
}
