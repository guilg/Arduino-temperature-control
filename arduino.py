import serial
from PyQt5.QtCore import QThread, pyqtSignal

# ----------------------------------------------------------------------------

class ArduinoThread(QThread):
    """ Define an Arduino object to be able to listen to the serial port and
    keep feeding the main program. Also used to send message to the Arduino.
    """
    data_changed = pyqtSignal(tuple)
    infos_signal = pyqtSignal(str)

    def __init__(self, port, baudrate, timeout):
        
        QThread.__init__(self)
        self.ser = serial.Serial(port, baudrate, timeout=timeout)
        self.ser.close()
        self.ser.open()
    
    def run(self):
        
        while self.ser.is_open:
            
            data = []
            incoming = self.ser.readline().decode()
            parts = incoming.split(" ")
            #print(parts)
            if parts[0] == "Data":
                data = (parts[1], parts[2], parts[3], parts[4])
                self.data_changed.emit(data)
            elif parts[0] == "Info":
                self.infos_signal.emit(incoming)

    def send(self, msg):
        """ Sends a message to the Arduino, and wait to process.
        """

        self.ser.write(str.encode(msg))
        self.ser.flush()

    def close(self):
        
        self.ser.close()