from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5 import QtNetwork

# ----------------------------------------------------------------------------

class TcpServer(QThread):
    """ Defines the thread listening to the go signal from MATLAB.
    """

    tcpsignal = pyqtSignal(str)

    def __init__(self, port=8237):
        
        QThread.__init__(self)

        self.port = port
        self.server = QtNetwork.QTcpServer()
        self.server.listen(QtNetwork.QHostAddress.LocalHost, port)
        self.server.newConnection.connect(self.addConnection)

    def addConnection(self):
        """ Add incoming connection and wait for message from client.
        """
        
        self.client = self.server.nextPendingConnection()
        self.client.readyRead.connect(self.receiveMessage)
        
        connected_ok = "Connected to localhost:" + str(self.port)
        self.tcpsignal.emit(connected_ok)

    def receiveMessage(self):
        """ Gets incoming message from TCPIP.
        """
        
        if self.client.bytesAvailable():
            received = self.client.readAll().data().decode()
            if received == "run":
                self.tcpsignal.emit(received)