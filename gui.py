import os
import sys
import time
import datetime
from PyQt5.QtWidgets import (QApplication, QWidget, QPushButton, QDesktopWidget, QLabel, QLineEdit, QGridLayout, QCheckBox, QButtonGroup, QLCDNumber, QFileDialog, QRadioButton)
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QTimer
from PyQt5 import QtNetwork
import pyqtgraph as pg
from protocol import Protocol
from server import TcpServer
from arduino import ArduinoThread

# ----------------------------------------------------------------------------

class TemperatureController(QWidget):
    """ Create a Qt graphical user interface (GUI) to control the temperature
    in the fish pool of the 1P setup.
    """
    
    def __init__(self):
        
        super(TemperatureController, self).__init__()
        
        # Defaults values
        self.root = "S:" + os.sep
        self.study = "Thermotaxis"
        self.defaultProtocol = os.path.join("C:" + os.sep, "Users", "Jean Perrin", "Documents", "Protocols", "Thermotaxis", "TemperatureController")
        self.defaultPort = "COM10"
        self.defaultBaud = 115200
        self.defaultTimeout = 1

        self.defaultT = 26
        self.defaultP = 65
        self.defaultI = 0.4
        self.defaultD = 20

        self.regulating = False

        # Get path
        self.getPath()

        # Call the GUI constructor
        self.initWindow()

        # Update path
        self.getPath()

        # Connect to localhost
        self.connectTCP(3332)

    def initWindow(self):
        """ Initialize the window, create buttons and place them.
        """
        
        # --- Grid ---
        grid = QGridLayout()
        grid.setSpacing(8)

        # --- Arduino box ---
        arduino_title = QLabel("Arduino", self)
        
        # Connection to Arduino
        btn_connect = QPushButton("Connect", self)
        btn_connect.setToolTip("Connect to the Arduino.")
        btn_connect.clicked.connect(self.connect)
        
        # Port
        port_text = QLabel("Port", self)
        self.port_edit = QLineEdit(self.defaultPort, self)
        self.port_edit.setFixedWidth(100)
        # Baudrate
        baud_text = QLabel("Baudrate", self)
        self.baud_edit = QLineEdit(str(self.defaultBaud), self)
        self.baud_edit.setFixedWidth(100)
        # Timeout
        time_text = QLabel("Timeout", self)
        self.timeout_edit = QLineEdit(str(self.defaultTimeout), self)
        self.timeout_edit.setFixedWidth(100)

        # Get info
        btn_info = QPushButton("Get info", self)
        btn_info.clicked.connect(self.askInfo)
        
        # --- Temperature box ---
        temperature_title = QLabel("Temperature", self)
        
        # Idle/Regulation/direct control
        self.btn_idle = QRadioButton("Idle", self)
        self.btn_idle.toggled.connect(self.setIdle)
        self.btn_idle.setChecked(True)
        self.btn_dc = QRadioButton("Direct control", self)        
        self.btn_regul = QRadioButton("Regulation", self)        

        # Make an exclusive group with these three checkboxes
        self.group_mode = QButtonGroup()
        self.group_mode.addButton(self.btn_dc)
        self.group_mode.addButton(self.btn_regul)
        self.group_mode.addButton(self.btn_idle)
        
        # Target temperature
        target_text = QLabel("Target", self)
        self.target_edit = QLineEdit(str(self.defaultT), self)
        self.target_edit.setFixedWidth(65)

        # Set target temperature
        btn_set_target = QPushButton("Set", self)
        btn_set_target.clicked.connect(self.setTarget)
        
        # Set and save
        btn_set_save = QPushButton("Set && Start saving", self)
        btn_set_save.clicked.connect(self.setTarget)
        btn_set_save.clicked.connect(self.startSaving)
        
        # PID coeffiecients
        P_text = QLabel("P", self)
        I_text = QLabel("I", self)
        D_text = QLabel("D", self)
        self.P = QLineEdit(str(self.defaultP), self)
        self.I = QLineEdit(str(self.defaultI), self)
        self.D = QLineEdit(str(self.defaultD), self)
        self.P.setFixedWidth(65)
        self.I.setFixedWidth(65)
        self.D.setFixedWidth(65)
        
        # Direct control percentages
        dc_hot_text = QLabel("Hot", self)
        self.dc_hot_edit = QLineEdit("50", self)
        self.dc_hot_edit.setFixedWidth(65)
        self.dc_hot_check = QCheckBox(self)
        dc_cold_text = QLabel("Cold", self)
        self.dc_cold_edit = QLineEdit("50", self)
        self.dc_cold_edit.setFixedWidth(65)
        self.dc_cold_check = QCheckBox(self)
        
        # Make an exclusive group with these two checkboxes
        self.group_dc = QButtonGroup()
        self.group_dc.addButton(self.dc_hot_check)
        self.group_dc.addButton(self.dc_cold_check)
        
        # --- Experiment box ---
        experiment_title = QLabel("Experiment", self)

        # Run
        run_text = QLabel("Run", self)
        self.run_edit = QLineEdit("1", self)
        self.run_edit.textChanged.connect(self.getPath)
        self.run_edit.returnPressed.connect(self.getPath)
        self.run_edit.setFixedWidth(65)

        # Protocol
        protocol_text = QLabel("Protocol", self)
        self.protocol_edit = QLineEdit(self.defaultProtocol)
        self.btn_protocol_browse = QPushButton("Browse", self)
        self.btn_protocol_browse.clicked.connect(self.browseProtocol)
        self.btn_protocol_load = QPushButton("Load", self)
        self.btn_protocol_load.clicked.connect(self.loadProtocol)
        duration_label = QLabel("Duration", self)
        self.duration_text = QLabel("", self)
        targets_label = QLabel("Targets", self)
        self.targets_text = QLabel("", self)
        timings_label = QLabel("Timings", self)
        self.timings_text = QLabel("", self)
        self.btn_protocol_run = QPushButton("Run", self)
        self.btn_protocol_run.setCheckable(True)
        self.btn_protocol_run.clicked.connect(self.runProtocol)

        # --- Temperature, cold junction and command LCD display ---
        lcd_temperature_label = QLabel("T")
        self.lcd_temperature = QLCDNumber(self)
        lcd_cj_label = QLabel("CJ")
        self.lcd_cj = QLCDNumber(self)
        lcd_command_label = QLabel("Cmd")
        self.lcd_command = QLCDNumber(self)
        
        # --- Log ---
        save_title = QLabel("Save file", self)
        self.save_path_edit = QLineEdit(self.day_path, self)
        self.btn_save_start = QPushButton("Start saving", self)
        self.btn_save_start.setCheckable(True)
        self.btn_save_start.clicked.connect(self.startSaving)
        btn_save_stop = QPushButton("Stop saving", self)
        btn_save_stop.clicked.connect(self.stopSaving)
        timer_text = QLabel("Timer (s)", self)
        self.timer_edit = QLineEdit("0")
        self.timer_edit.setFixedWidth(65)
        
        # --- Plot ---
        self.plot_canvas = pg.PlotWidget(title="Monitor")
        self.plot_canvas.setLabel("bottom", "Time", "s")
        self.plot_canvas.setLabel("left", "Temperature", "°C")
        self.plot_canvas.showGrid(x = True, y = True)
        
        # --- General ---
        btn_quit = QPushButton("Quit", self)
        btn_quit.clicked.connect(self.quit)
        self.message_to_user = QLabel(self)
        btn_tcp = QPushButton("Connect TCP", self)
        btn_tcp.clicked.connect(self.connectTCP)
        
        # --- Organize elements on grid layout ---
        # - Arduino box
        grid.addWidget(arduino_title, 0, 0, 1, 2)
        grid.addWidget(port_text, 1, 0, 1, 1)
        grid.addWidget(self.port_edit, 1, 1, 1, 1)
        grid.addWidget(baud_text, 2, 0, 1, 1)
        grid.addWidget(self.baud_edit, 2, 1, 1, 1)
        grid.addWidget(time_text, 3, 0, 1, 1)
        grid.addWidget(self.timeout_edit, 3, 1, 1, 1)
        grid.addWidget(btn_connect, 4, 0, 1, 1)
        
        # - Temperature box
        grid.addWidget(temperature_title, 0, 3, 1, 5)
        # Idle
        grid.addWidget(self.btn_idle, 1, 3, 1, 3)
        # Direct control
        grid.addWidget(self.btn_dc, 2, 3, 1, 3)
        grid.addWidget(dc_hot_text, 3, 3, 1, 1)
        grid.addWidget(self.dc_hot_edit, 3, 4, 1, 1)
        grid.addWidget(self.dc_hot_check, 3, 5, 1, 1)
        grid.addWidget(dc_cold_text, 4, 3, 1, 1)
        grid.addWidget(self.dc_cold_edit, 4, 4, 1, 1)
        grid.addWidget(self.dc_cold_check, 4, 5, 1 ,1)
        # Regulation
        grid.addWidget(self.btn_regul, 1, 6, 1, 2)
        grid.addWidget(target_text, 2, 6, 1, 1)
        grid.addWidget(self.target_edit, 2, 7, 1, 1)
        grid.addWidget(P_text, 3, 6, 1, 1)
        grid.addWidget(self.P, 3, 7, 1, 1)
        grid.addWidget(I_text, 4, 6, 1, 1)
        grid.addWidget(self.I, 4, 7, 1, 1)
        grid.addWidget(D_text, 5, 6, 1, 1)
        grid.addWidget(self.D, 5, 7, 1, 1)
        # Set buttons
        grid.addWidget(btn_set_target, 6, 4, 1, 4)
        grid.addWidget(btn_set_save, 7, 4, 1, 4)

        # - Experiment box
        grid.addWidget(experiment_title, 0, 9, 1, 3)
        # Run
        grid.addWidget(run_text, 1, 9, 1, 1)
        grid.addWidget(self.run_edit, 1, 10, 1, 1)
        # Protocol
        grid.addWidget(protocol_text, 2, 9, 1, 1)
        grid.addWidget(self.protocol_edit, 2, 10, 1, 2)
        grid.addWidget(self.btn_protocol_browse, 3, 10, 1, 1)
        grid.addWidget(self.btn_protocol_load, 3, 11, 1, 1)
        # Protocol parameters
        grid.addWidget(duration_label, 4, 9, 1, 1)
        grid.addWidget(self.duration_text, 4, 10, 1, 2)
        grid.addWidget(targets_label, 5, 9, 1, 1)
        grid.addWidget(self.targets_text, 5, 10, 1, 2)
        grid.addWidget(timings_label, 6, 9, 1, 1)
        grid.addWidget(self.timings_text, 6, 10, 1, 2)
        # Protocol launcher
        grid.addWidget(self.btn_protocol_run, 7, 9, 1, 3)

        # - LCD
        grid.addWidget(lcd_temperature_label, 0, 12, 1, 1)
        grid.addWidget(self.lcd_temperature, 1, 12, 1, 1)
        grid.addWidget(lcd_cj_label, 2, 12, 1, 1)
        grid.addWidget(self.lcd_cj, 3, 12, 1, 1)
        grid.addWidget(lcd_command_label, 4, 12, 1, 1)
        grid.addWidget(self.lcd_command, 5, 12, 1, 1)
        
        # - Save box
        grid.addWidget(save_title, 5, 0, 1, 2)
        grid.addWidget(self.save_path_edit, 6, 0, 1, 2)
        grid.addWidget(self.btn_save_start, 7, 0, 1, 1)
        grid.addWidget(btn_save_stop, 7, 1, 1, 1)
        grid.addWidget(timer_text, 6, 2, 1, 1)
        grid.addWidget(self.timer_edit, 7, 2, 1, 1)
        
        # - Plot box
        grid.addWidget(self.plot_canvas, 8, 0, 10, 13)
        
        # - Misc
        grid.addWidget(btn_tcp, 19, 11, 1, 1)
        grid.addWidget(btn_quit, 19, 12, 1, 1)
        grid.addWidget(self.message_to_user, 19, 1, 1, 8)
        grid.addWidget(btn_info, 19, 0, 1, 1)
        
        # Set up the layout
        self.setLayout(grid)
        
        # --- Window        
        self.setGeometry(300, 300, 1000, 800)
        self.setWindowTitle("TemperatureController")
        if sys.platform == 'win32':
            self.setWindowIcon(QIcon(os.path.join("Icon", "ThermoMeter.ico")))
        else:
            self.setWindowIcon(QIcon(os.path.join("Icon", "ThermoMeter.png")))

        
        # Show window
        self.moveWindow()
        self.show()
        
    def moveWindow(self):
        """ Center the window on the screen.
        """
        
        qr = self.frameGeometry()
        self.cp = QDesktopWidget().availableGeometry().center()
        qr.moveCenter(self.cp)
        self.move(qr.topLeft())
        
    def connect(self):
        """ Establish the connection to the Arduino and initialize plot.
        """

        # Gather connection parameters
        port = self.port_edit.text()
        baudrate = float(self.baud_edit.text())
        timeout = float(self.timeout_edit.text())
        
        # Connect
        if hasattr(self, "ardthread"):
            self.ardthread.terminate()
            self.ardthread.close()
            self.plot_canvas.removeItem(self.curve)

        try:
            self.ardthread = ArduinoThread(port, baudrate, timeout)
            self.printToUser("Connected to Arduino.")
        except Exception as e:
            self.printToUser("Error while connecting to Arduino : " + str(e))
            return
        
        # Initialize plot counter
        self.plot_counter = 0
        
        # Start thread for getting new data from Arduino
        self.ardthread.data_changed.connect(self.updatePlot)
        self.ardthread.infos_signal.connect(self.catchInfo)
        self.ardthread.start()
        self.curve = self.plot_canvas.plot()
        self.x_data = []
        self.y_data = []
        self.first_time = True

        # Update path
        self.getPath()

    def setIdle(self, state):
        """ Set the temperature regulation mode to idle.
        """
        
        if self.btn_idle.isChecked() and hasattr(self, "ardthread"):
            self.send("stop")
            self.printToUser("Idle mode")
        
    def setTarget(self):
        """ Main instruction to the Arduino : set the target temperature and PID
        coefficients, and the regulation mode (feedback or direct control).
        """
        
        if not hasattr(self, "ardthread"):
            self.printToUser("Not connected to Arduino.")
            return

        if self.btn_idle.isChecked():
            # --- Idle mode
            self.send("stop")
            self.regulating = False
            self.printToUser("Idle mode")
            
        elif self.btn_dc.isChecked():
            # --- Direct control mode
            
            if self.dc_hot_check.isChecked():
                hot_value = int(float(self.dc_hot_edit.text())*255/100)

                self.send("H " + str(hot_value))
                self.regulating = False
                self.printToUser("Direct control mode, heating at " + self.dc_hot_edit.text() + "%")
                
            elif self.dc_cold_check.isChecked():
                cold_value = int(float(self.dc_cold_edit.text())*255/100)
                
                self.send("C " + str(cold_value))
                self.regulating = False
                self.printToUser("Direct control mode, cooling at " + self.dc_cold_edit.text() + "%")

        elif self.btn_regul.isChecked():
            # --- Regulation mode

            # Regulation parameters
            Tcmd = "T " + self.target_edit.text()
            Pcmd = "P " + self.P.text()
            Icmd = "I " + self.I.text()
            Dcmd = "D " + self.D.text()
            self.send(Tcmd + "|" + Pcmd + "|" + Icmd + "|" + Dcmd)
            
            # If target = 0, set idle
            if not float(self.target_edit.text()):
                self.setIdle()
                return

            # Start regulation if not active already
            if not self.regulating:
                time.sleep(.01)
                self.send("start")
                self.regulating = True

            # Verbose
            self.printToUser("Regulation to " + self.target_edit.text() + "°C")
            
        else:
            
            # Stop regulation
            self.send("stop")
            
            # Verbose
            self.printToUser("Idle mode")
            
    
    def startSaving(self):
        """ Create a file according to the input filename and write a simple
        header.
        """
        
        file = self.save_path_edit.text()
        c = 0
        
        if not self.btn_save_start.isChecked():
            self.btn_save_start.toggle()
        
        # Check if directory exists
        if not os.path.isdir(os.path.dirname(file)):
            self.printToUser("Directory doesn't exist.")
            self.btn_save_start.toggle()
            return

        # Check if file already exists and create backup
        if os.path.isfile(file):
            # Create a backup of existing file
            backup_file = file + str(c) + ".bak"
            while os.path.isfile(backup_file):
                c += 1
                backup_file = file + str(c) + ".bak"
        
            os.rename(file, backup_file)

        # Open file and write header
        self.fid = open(file, 'a')
        self.fid.write(str(datetime.datetime.now()) + "\n")
        self.fid.write("Target = " + self.target_edit.text() + ", P = " + self.P.text() + ", I = " + self.I.text() + ", D = " + self.D.text() + "\n")
        self.fid.write("Time\tTemperature\n")
        
        # Reset if it is not the first save
        self.first_time = True
        if hasattr(self, 'start_time'):
            delattr(self, 'start_time')
            
        
    def stopSaving(self):
        """ Stop temperature logging in file.
        """
        
        if self.btn_save_start.isChecked():
            self.btn_save_start.toggle()
        else:
            return

        self.fid.close()
        
    def printToUser(self, message):
        """ Verbose user within the GUI.
        """
        
        self.message_to_user.setText(message)
        
    def updatePlot(self, data):
        """ Update the plot listening to new incoming data, and write data to 
        specified file if wanted. Input data is connected to the Arduino thread.
        """
        
        t = float(data[0])
        temperature = float(data[1])
        cjtemp = float(data[2])
        command = float(data[3])
        
        if len(self.x_data) < 600:
            # Build list for the first time
            self.x_data.append(t/1000)
            self.y_data.append(temperature)
        else:
            # Replace elements
            self.x_data.pop(0)
            self.x_data.append(t/1000)
            self.y_data.pop(0)
            self.y_data.append(temperature)
        
        # Write data to text file if required
        if self.btn_save_start.isChecked():
            
            # With timer
            if float(self.timer_edit.text()) > 0:
                if not hasattr(self, "start_time"):
                    self.start_time = time.time()
                else:
                    
                    # Record for defined duration
                    if time.time() - self.start_time < float(self.timer_edit.text()):
                        # Origin of time
                        if self.first_time:
                            self.t0 = t
                            self.first_time = False
                            
                        # Write to file
                        self.fid.write("%.1f" % (t - self.t0) + "\t" + "%.8f" % (temperature) + "\n")
                    else:
                        self.stopSaving()
                        delattr(self, "start_time")
                            
            # Record until the stop button is pressed
            else:
                # Origin of time
                if self.first_time:
                    self.t0 = t
                    self.first_time = False
        
                # Write to file
                self.fid.write("%.1f" % (t - self.t0) + "\t" + "%.8f" % (temperature) + "\n")
        else:
            self.first_time = False

        # Update plot
        self.curve.setData(self.x_data, self.y_data)
        
        # Update LCDs
        self.lcd_temperature.display(temperature)
        self.lcd_cj.display(cjtemp)
        self.lcd_command.display(command)

    def send(self, msg):
        """ Send a command via the serial connection
            INPUTS
            ------
            msg : string corresponding to a message for the Arduino.
        """
        
        if not hasattr(self, "ardthread"):
            self.printToUser("Not connected to Arduino.")
            return
        
        self.ardthread.send(msg)

    def browseProtocol(self):
        """ Open UI to choose a file.
        """

        filename = QFileDialog.getOpenFileName(self, 'Open file', self.protocol_edit.text())

        if filename[0]:
            self.protocol_edit.setText(filename[0])
            self.loadProtocol()

    def loadProtocol(self):
        """ Load protocol file via Protocol class.
        """

        self.protocol = Protocol(self.protocol_edit.text())
        
        if not self.protocol.error:
            # Get total duration
            self.duration_text.setText(str(sum(self.protocol.duration)) + "s")
            self.targets_text.setText(str(self.protocol.T).strip("[]") + "°C")
            self.timings_text.setText(str(self.protocol.duration).strip("[]") + "s")
            self.timer_edit.setText(str(sum(self.protocol.duration)))
            self.printToUser("Protocol loaded.")
        else:
            self.printToUser("Error while loading protocol.")

    def runProtocol(self):
        """ Run protocol with QTimer.
        """

        if not hasattr(self, "protocol"):
            self.printToUser("No protocol loaded.")
            self.btn_protocol_run.setChecked(False)
            return

        if self.protocol.error:
            self.printToUser("Protocol did not load properly.")
            self.btn_protocol_run.setCheched(False)
            return

        # Check if protocol already running and stop it if yes
        if not self.btn_protocol_run.isChecked():
            self.protocol.T = []

        # Check if protocol is already running or is finished.
        if not self.protocol.T:
            if self.btn_protocol_run.isChecked():
            	self.btn_protocol_run.setChecked(False)
            self.stopSaving()
            if hasattr(self, "protocol_timer"):
                self.protocol_timer.stop()
            self.loadProtocol()
            self.printToUser("Protocol finished, protocol reloaded.")
            return
        elif len(self.protocol.T) == self.protocol.size:
            self.printToUser("Protocol running...")
            self.btn_save_start.toggle()
            self.startSaving()

        if not self.btn_regul.isChecked():
        	self.btn_regul.toggle()

        # Send protocol command to Arduino
        T = self.protocol.T[0]
        P = self.protocol.P[0]
        I = self.protocol.I[0]
        D = self.protocol.D[0]

        # Update text field in the GUI
        self.target_edit.setText(str(T))
        self.P.setText(str(P))
        self.I.setText(str(I))
        self.D.setText(str(D))

        # Send command to Arduino
        self.setTarget()

        # Reexectute the run after given time
        self.protocol_timer = QTimer()
        self.protocol_timer.timeout.connect(self.runProtocol)
        self.protocol_timer.setSingleShot(True)
        self.protocol_timer.start(self.protocol.duration[0]*1000)
        self.protocol.duration.pop(0)
        self.protocol.T.pop(0)
        self.protocol.P.pop(0)
        self.protocol.I.pop(0)
        self.protocol.D.pop(0)
        
    def sendInstruction(self):
        """ Sends instruction from runProtocol.
        """

        self.send(self.protocol_cmd)

    def connectTCP(self, port=8237):
        """ Create object to listen to TCP port.
        """

        self.server = TcpServer(port)
        self.server.tcpsignal.connect(self.handleTcpMessage)

    def handleTcpMessage(self, message):
        """ Choose what to do with input message from TCP.
        """

        if "run" in message:
            self.printToUser("Run message received")
            self.btn_protocol_run.setChecked(True)
            self.runProtocol()
        elif "Connected to" in message:
            self.printToUser(message)

    def askInfo(self):
        """ Ask Arduino for info.
        """
        
        if not hasattr(self, "ardthread"):
            self.printToUser("Not connected to Arduino.")
            return

        self.send("info")

    def catchInfo(self, message):
        """ Wait for information from ArduinoThread.
        """

        _, mode, target, P, I, D = message.split(" ")
        msg = mode + " Target : " + target + ", P : " + P + ", I : " + I + ", D : " + D
        self.printToUser(msg)

    def getPath(self):
        """ Get the full path to today's date and to the specified run if any.
        """

        today = time.strftime("%Y-%m-%d")
        self.day_path = os.path.join(self.root, self.study, today)

        if hasattr(self, "run_edit"):

            try:
                self.run_path = os.path.join(self.day_path, "Run " + "%02i" % float(self.run_edit.text()), "Thermostat.txt")
            except ValueError:
                self.run_path = self.day_path
            
            self.save_path_edit.setText(self.run_path)


    def quit(self):
        """ Quit program, closing serial connection if it exists.
        """

        if hasattr(self, "ardthread"):
            self.send("stop")
            self.ardthread.terminate()
            self.ardthread.ser.close()
        self.close()
        
if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    TC = TemperatureController()
    sys.exit(app.exec_())