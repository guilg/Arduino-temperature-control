import os

# ----------------------------------------------------------------------------

class Protocol():
    """ Defines the protocol class to get parameters from a text file.
    """

    def __init__(self, filename):
        """ Constructor.
        INPUT :
        -----
        filename : str, path to the protocol file.
        """

        # Load file
        if os.path.isfile(filename):
            self.fid = open(filename, 'r')
        
            # Initialize protocol parameters
            self.T = []
            self.P = []
            self.I = []
            self.D = []
            self.duration = []

            # Parse file
            self.parse()
            self.error = False
        else:
            self.error = True

    def parse(self):
        """ Parse file to get timings and targets.
        """
        # Parse file
        if self.fid.closed:
            self.fid = open(self.fid.name)

        for line in self.fid:

            if not line.startswith("#"):
                parts = line.split(":")

                if parts[0] == "target":
                    self.T.append(float(parts[1]))
                elif parts[0] == "P":
                    self.P.append(float(parts[1]))
                elif parts[0] == "I":
                    self.I.append(float(parts[1]))
                elif parts[0] == "D":
                    self.D.append(float(parts[1]))
                elif parts[0] == "wait":
                    self.duration.append(float(parts[1]))
        
        self.size = len(self.T)
        self.fid.close()